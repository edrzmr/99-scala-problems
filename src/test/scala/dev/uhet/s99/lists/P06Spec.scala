package dev.uhet.s99.lists

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class P06Spec extends AnyFlatSpec with Matchers {

  "P06 isPalindrome" should "Find out whether a list is a palindrome" in {
    p06.isPalindrome(List(1, 2, 3, 4)) should be (false)
    p06.isPalindrome(List(1, 2, 2, 1)) should be (true)
    p06.isPalindrome(List(1, 2, 1)) should be (true)
    p06.isPalindrome(List.empty) should be (true)
    p06.isPalindrome(Nil) should be (true)
  }
}
