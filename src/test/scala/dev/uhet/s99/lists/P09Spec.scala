package dev.uhet.s99.lists

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class P09Spec extends AnyFlatSpec with Matchers {

  "P09 takeWhileEqual" should "works" in {
    p09.takeWhileEqual(List(1, 1, 1, 2, 3)) should be((List(1, 1, 1), List(2, 3)))
    p09.takeWhileEqual(List(1, 2, 3)) should be((List(1), List(2, 3)))
  }

  "P09 takeWhile" should "works" in {
    p09.takeWhile(List(1, 1, 1, 2, 3))(_ != _) should be((List(1, 1, 1), List(2, 3)))
    p09.takeWhile(List(1, 2, 3))(_ != _) should be((List(1), List(2, 3)))
    p09.takeWhile(List(1))(_ != _) should be((List(1), List()))
    p09.takeWhile(List.empty[Int])(_ != _) should be((List(), List()))
  }

  "P09 pack" should "Pack consecutive duplicates of list elements into sublists" in {
    p09.pack(List(1, 1, 3, 2, 2, 1)) should be (List(List(1, 1), List(3), List(2, 2), List(1)))
  }
}
