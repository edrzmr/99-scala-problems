package dev.uhet.s99.lists

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class P10Spec extends AnyFlatSpec with Matchers {

  "P10 length enconding of list" should "works" in {
    p10.encode(List(1, 1, 3, 2, 2, 1)) should be (List((2, 1), (1, 3), (2, 2), (1, 1)))
  }
}
