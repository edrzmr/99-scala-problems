package dev.uhet.s99.lists

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class P07Spec extends AnyFlatSpec with Matchers {

  "P07 concat" should "Concatenate 2 lists" in {
    p07.concat(List(1, 2), List(3, 4)) should be(List(1, 2, 3, 4))
  }

  "P07 flatten" should "Flatten a nested list structure" in {
    p07.flatten(List(List(1, 2), List(3, 4))) should be(List(1, 2, 3, 4))
  }
}
