package dev.uhet.s99.lists

import scala.annotation.tailrec

object p02 {

  @tailrec
  def penultimate[A](list: List[A]): Option[A] = list match {
    case Nil => None
    case _ :: Nil => None
    case x :: _ :: Nil => Some(x)
    case _ :: xs => penultimate(xs)
  }
}
