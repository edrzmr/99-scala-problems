package dev.uhet.s99.lists

import scala.annotation.tailrec

object p11 {

  def encodeModified[A](list: List[A]): List[Any] = {

    @tailrec
    def inner(packed: List[List[A]], acc: List[Any]): List[Any] = packed match {
      case Nil => p05.reverse(acc)
      case x :: xs =>
        if (x.length > 1) inner(xs, (x.length, x.head) :: acc)
        else inner(xs, x.head :: acc)
    }

    inner(p09.pack[A](list), List.empty[(Int, A)])
  }
}
